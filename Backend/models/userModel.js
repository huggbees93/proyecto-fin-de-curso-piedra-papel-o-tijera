var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/finalProyect');
var Schema = mongoose.Schema;
var cardSchema = require("./cardModel").cardSchema;
mongoose.model('card', cardSchema);

var userSchema = new mongoose.Schema({
    username: "string",
    password: "string",
    email: "string",
    img: "string",
    card: [{type: Schema.Types.ObjectId, ref:'card'}]
});

var userModel = mongoose.model('user', userSchema);

module.exports = {
    userModel: userModel,
    userSchema: userSchema
};