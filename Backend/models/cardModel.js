var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/finalProyect");


var cardSchema = new mongoose.Schema({

    img: 'string',
    name: 'string'
})

var cardModel = mongoose.model('card', cardSchema);

module.exports = {
    cardModel: cardModel,
    cardSchema: cardSchema
}