var express = require('express');
var router = express.Router();
var jst = require('jsonwebtoken');

var userModel = require('../models/userModel').userModel;
var key = "enov343ewniew135f32g43g44";


router.post('/', function(req, res){

    userModel.findOne({username: req.body.name, password:req.body.password}).then(function(user){
        if(user){
            var userdata = {
                id: user._id,
                username: user.username,
                email: user.email
            }
           var token =  jst.sign(userdata, key);
           res.json({token:token});
        }
        else{
            res.status(400);
            res.send('bad credentials');
        }
    })
})

router.post('/validate', function(req, res){
    res.write(JSON);
}) 

router.post('/register', function(req, res){
    var user = new userModel({
        username: req.body.name,
        password: req.body.password,
        email:req.body.email
    })
    user.save().then(function(newUser){
        var token = jst.sign({
            id:newUser._id,
            username:newUser.username,
            email:newUser.email
        },key);
        res.json({token:token});
    })
    
})

module.exports = {router, key};