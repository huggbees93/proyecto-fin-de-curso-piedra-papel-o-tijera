var express = require('express');
var cardModel = require('../model/cardModel').cardModel;
var router = express.Router();

router.get('/', function(req, res){
    cardModel.find().then(function(cards){
        res.json(cards)
    })
})

router.get('/:id', function(req, res){
    cardModel.findById(req.params.id).then(function(card){
        res.json(card)
    })
})

router.post('/', function(req, res){
    var card = new cardModel({
        name:req.body.name,
        img: req.body.img_url
    })
    card.save().then(function(card){
        res.json(card);
    })
})

router.delete('/:id', function(req,res){

    cardModel.deleteOne({_id:req.params.id}).then(function(){
        res.json({});
    });
})

router.put('/:id', function(req, res){

    cardModel.findById(req.params.id).then(function(card){
        card.name = req.body.name;
        card.img = req.body.img;

        card.save().then(function(u){
            res.json(card);
        })
        
    })
});

router.patch('/:id', function(req, res){

    cardModel.findById(req.params.id).then(function(card){
        card.name = req.body.name ? req.body.name : card.name;
        card.img = req.body.img ? req.body.img : card.img;

        card.save().then(function(u){
            res.json(card);
        })
        
    })
});

module.exports = router;
