var express = require('express');
var userModel = require('../models/userModel').userModel;
const bodyParser = require('body-parser');
var router = express.Router();


router.get('/', function(req,res){
    userModel.find().then(function(usuarios){
        //usuarios = usuarios.filter(user => user.id == req.user.id)
        res.json(usuarios);
    })
});

router.get('/:id', function(req, res){
    userModel.findById(req.params.id).then(function(usuario){
        res.json(usuario);
    })
})

router.post('/', function(req,res){
    var user = new userModel({
        username: req.body.name,
        password: req.body.password,
        email:req.body.email,
        card: req.body.card.id
    })
    user.save().then(function(u){
        res.json(u)
    })
})

router.delete('/:id', function(req,res){
    userModel.findByIdAndDelete(req.params.id).then(function(){
        res.json();
    })
})
router.put('/:id', function(req, res){
    userModel.findById(req.params.id).then(function(user){
        user.username = req.body.username ? req.body.username : user.username;
        user.email = req.body.email ? req.body.email : user.email;
        user.password = req.body.password ? req.body.password : user.password;
        user.card = req.body.card ? req.body.card : user.card;
    })
})


module.exports = router;
