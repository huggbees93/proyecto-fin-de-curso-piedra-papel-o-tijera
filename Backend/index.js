var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
var cors = require('cors');

var userRouter = require('./controllers/userController');
var key = require('./controllers/logInController').key;
var logInRouter = require('./controllers/logInController').router;


var app = express();
app.use(cors());

var credentials = function(req, res, next){
    if(req.url == '/login/register' || req.url == '/login'){
        next();
    }
    else{
        try{
            var user = jwt.verify(req.header('token'), key);
            req.user = user;
            next();
        }
        catch{
            res.status(400);
            res.send('no good credentials');
        }
    }
}

//no estaba andando en postman, cuando pongo un token correcto y pongo get users se queda cargando hasta que se rinde y no carga.


app.use(bodyParser.urlencoded());


app.use(credentials);
app.use('/users', userRouter);
app.use('/login', logInRouter);

app.listen(8000);

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'example.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}