import React, { Component } from 'react';
//import {Link} from "react-router-dom";
import './Login.scss';

class Login extends Component {
  state = {
    name:"",
    password: "",
    error: null
  }

  cuandoCambie = event =>{
    this.setState({
    [event.target.name]: event.target.value
    });
  }
  onSubmit = event =>{
    event.preventDefault();
    const url = "http://localhost:8000/login";

    const myInit = {
      method: 'POST',
      body: JSON.stringify({
        name: this.state.name,
        password:this.state.password
    })};

    fetch(url, myInit)
    .then(respuesta =>{
      console.log('respuesta', respuesta);
      //const userToken = Storage.setItem('userToken',respuesta);
      //asumiendo que funciona el fetch, cuando vuelve el token reviso aca si trajo o no un token, si no trajo nada quiero
      // que salte un banner que no esta registrado con un link para que se registre, sino que vaya al home.
      // No se si esta bien, revisar si funciona asi el return true y false.
      /*if (!userToken) {
        console.log('no tiene token');
        return false;
      }
      else{
      
      return true;
      }*/
    })
    .catch(error =>{
      this.setState({error :'no se conecta a la api'});
      console.log('error', error);
    });
  }



  render() {
    const {name, password} = this.state;

    return (
      <div className="Login">
        <header className="Login-header">
          <div>
          </div>
        </header>
        <h1>Nerdos Battle</h1>
        <form onSubmit = {this.onSubmit}>
          <h4>username</h4>
          <input 
          type = "text"
          id = "name"
          name = "name"
          defaultValue = {name}
          onSubmit = {this.cuandoCambie}
         ></input>
         <br />
         <h4>Password</h4>
         <input 
          type = "password"
          id = "password"
          name = "password"
          defaultValue = {password}
          onSubmit = {this.cuandoCambie}
         ></input>

          <br />
          <input type = 'submit' value = 'login'></input>

        </form>

      </div>
    );
  }
}

export default Login;
