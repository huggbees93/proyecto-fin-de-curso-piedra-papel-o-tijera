import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from './Home';
import Login from './Login'


class App extends Component {
  
  
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <>
            <Route exact path = "/" component = {Home}/>
            <Route exact path = "/home" component = {Home}/>
            <Route path = "/login" component = {Login}/>
          </>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
