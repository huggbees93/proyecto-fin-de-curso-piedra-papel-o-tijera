import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { Avatar } from '../components/Avatar';
//assets
import './Home.scss';
import banner from '../assets/banner.png';
import { PlayButton } from '../components/PlayButton';


// De principio la elecion de que vas a jugar va a ser aca.
// Tengo que guardarla y luego poder llamarla en match.
class Home extends Component {


  //<img src = {banner} alt = 'banner'></img>

  render() {
    return (
      <div className="App">
        <div className = 'banner'>
        
        </div>
        <header className="App-header">
          <div className = 'logOut'>
            <Link to="/login">Log out</Link>
          </div>
        </header>
        <div className = "left-side">
          <div className = 'avatarPic'>
            <Avatar src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1fG-_EazvrTXfai-znLjIHpeWQjozTeEjMs5A_WbsZCxvRTYJ" alt = "avatar img" >
            </Avatar>
          </div>
          <PlayButton></PlayButton>
        </div>
        <div className = "right-side">
          <div className = "rankTitle">
            <h1>World Rank</h1>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
