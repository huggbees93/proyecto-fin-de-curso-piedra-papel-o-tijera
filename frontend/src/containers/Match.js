import React, {Component} from 'react';
import './Match.scss';


// tengo que llamar aca las elecciones de los dos jugadores y hacer una funcion que se fije quien gana, y que eso dispare un banner
// diciendo quien gano
class Match extends Component{
    state = {
        choice: '',
        enemyChoice: '',
        choiceImg:''
    }

    winCondition =(playerChoice, otherChoice)=>{
      if(playerChoice === otherChoice){
        alert("draw");
      }
      else if(playerChoice === "rock" && otherChoice === 'scissor'){

        alert("you win");
      }
      else if(playerChoice === "paper" && otherChoice === 'rock'){

        alert("you win");
      }
      else if(playerChoice === "scissor" && otherChoice === 'paper'){
        
        alert("you win");
      }
      else{
        alert("you lost");
      }
    }
    
    render(){

        return(
          <h1>Battle!</h1>
        )
    }
}

export default Match;