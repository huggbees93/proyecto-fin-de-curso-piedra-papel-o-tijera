// Dependencies
import React, { Component } from "react";

// Assets
import "./Avatar.scss";

class Avatar extends Component {
  state = {
    alt: null,
    src: null
  };

  componentWillMount() {
    this.setState({
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6usA-fTHoWrpJ8DWuNwn25qf4y6B7obKt0yd6n5y-WqN_RS0GPg"
    });
  }

  render() {
    const { alt, url: src } = this.props;

    return (
      <div className="Avatar">
        <img src={src} alt={alt} className="img" />
      </div>
    );
  }
}

export { Avatar };
