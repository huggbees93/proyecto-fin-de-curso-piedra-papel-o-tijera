import React, {Component} from 'react';

import "./PlayButton.scss";

class PlayButton extends Component {

    render(){
        return(
            <div className = 'playBorder'>
            <button className = 'play'></button>
            </div>
        );
    }

}

export {PlayButton};